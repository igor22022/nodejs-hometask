const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    create(fighter) {
        const item = FighterRepository.create(fighter);
        if (!item) {
            return null;
        } else {
            return item;
        }
    };

    findAll() {
        const items = FighterRepository.getAll();
        if(!items || items.length === 0) {
            return null;
        }
        return items;
    }

    update(fighterId, dataToUpdate) {
        const item = FighterRepository.update(fighterId, dataToUpdate);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(fighterId) {
        const item = FighterRepository.delete(fighterId);
        if(!item) {
            return null;
        }
        return item;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();