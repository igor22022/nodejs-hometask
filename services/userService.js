const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
      
    create(user) {
        const item = UserRepository.create(user);
        if (!item) {
            return null;
        } else {
            return item;
        }
    };

    findAll() {
        const items = UserRepository.getAll();
        if(!items || items.length === 0) {
            return null;
        }
        return items;
    }

    update(userId, dataToUpdate) {
        const item = UserRepository.update(userId, dataToUpdate);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(userId) {
        const item = UserRepository.delete(userId);
        if(!item) {
            return null;
        }
        return item;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();