const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    const result = res.data;
    if (result) {
        res.status(200).send(result);
    } else {
        let error = {
            error: true,
            message: res.err.message || 'Error occurred. Try later.'
        }
        res.status(400).send(error);
    }

}

exports.responseMiddleware = responseMiddleware;