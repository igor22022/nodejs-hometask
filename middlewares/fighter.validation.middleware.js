const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation

    let requestBody = req.body;
    let validFighterData = true;
    let fighterRequestData = {};

    validate(requestBody);

    if (validFighterData) {
        next();
    } else {
        res.status(400).send({
            error: true,
            message: 'Data for creating a fighter is not valid'
        });
    }

    function validate (fighterData) {
        console.log(typeof fighterData.power);
        validFighterData = (fighterData.power && typeof fighterData.power === 'number' && fighterData.power < 100) &&
                            (fighterData.defense && typeof fighterData.defense === 'number' && fighterData.defense < 100) &&
                            (fighterData.defense && typeof fighterData.name === 'string' && fighterData.name.length > 0);

        // leave redundent properties out of req.body
        if (validFighterData) {
            for (let property in fighter) {
                if (property !== 'id') {
                    fighterRequestData[property] = requestBody[property];
                }
            }
            req.body = fighterRequestData;
        }
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    let requestBody = req.body;
    let validFighterData = true;
    let fighterRequestData = {};

    validate(requestBody);

    if (validFighterData) {
        next();
    } else {
        res.status(400).send({
            error: true,
            message: 'Fighter data to update is not valid'
        });
    }

    function validate (fighterData) {
        validFighterData = !(typeof fighterData.name !== 'undefined' && fighterData.name.length === 0) &&
                            !(typeof fighterData.health !== 'undefined' && typeof fighterData.health === 'number') &&
                            !(typeof fighterData.power !== 'undefined' && typeof fighterData.power === 'number' && fighterData.power < 100) &&
                            !(typeof fighterData.defense !== 'undefined' && typeof fighterData.defense === 'number' && fighterData.defense < 100);

        if (validFighterData) {
            for (let property in fighter) {
                if (property !== 'id' && requestBody.hasOwnProperty(property)) {
                    fighterRequestData[property] = requestBody[property];
                }
            }
            req.body = fighterRequestData;
        }
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;