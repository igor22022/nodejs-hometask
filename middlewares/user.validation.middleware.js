const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    let requestBody = req.body;
    let validUserData = true;
    let allNecessaryProps = true;
    let userRequestData = {};
    for (let key in user) {
        if (key !== 'id' && !requestBody.hasOwnProperty(key)) {
            allNecessaryProps = false;
            break;
        }
    }

    if (allNecessaryProps) {
        validate(requestBody);
    } else {
        validUserData = false;
    }

    if (validUserData) {
        next();
    } else {
        res.status(400).send({
            error: true,
            message: 'User entity to create is not valid'
        });
    }

    function validate (userData) {
        let mailChecker = /([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@gmail([\.])com/g;
        let phoneChecker = /^\+380[0-9]{9}/;

        validUserData = mailChecker.test(userData.email) &&
                        phoneChecker.test(userData.phoneNumber) &&
                        userData.password.length > 2 &&
                        userData.firstName.length > 0 &&
                        userData.lastName.length > 0;

        // leave redundent properties out of req.body
        if (validUserData) {
            for (let property in user) {
                if (property !== 'id') {
                    userRequestData[property] = requestBody[property];
                }
            }
            req.body = userRequestData;
        }
    } 
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    let requestBody = req.body;
    let validUserData = true;
    let userRequestData = {};

    validate(requestBody);

    if (validUserData) {
        next();
    } else {
        res.status(400).send({
            error: true,
            message: 'User entity to update is not valid'
        });
    }

    function validate (userData) {
        let mailChecker = /([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@gmail([\.])com/g;
        let phoneChecker = /^\+380[0-9]{9}/;
        validUserData = userData.email ? mailChecker.test(userData.email) : true &&
                        userData.phoneNumber ? phoneChecker.test(userData.phoneNumber) : true &&
                        !(typeof userData.password !== 'undefined' && userData.password.length < 3) &&
                        !(typeof userData.firstName !== 'undefined' && userData.firstName.length === 0) &&
                        !(typeof userData.lastName !== 'undefined' && userData.lastName.length === 0);

        // leave redundent properties out of req.body
        if (validUserData) {
            for (let property in user) {
                if (property !== 'id' && requestBody.hasOwnProperty(property)) {
                    userRequestData[property] = requestBody[property];
                }
            }
            req.body = userRequestData;
        }
    } 
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;