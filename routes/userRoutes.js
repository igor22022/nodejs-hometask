const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', function(req, res, next) {
  const result = UserService.findAll();
  if (result) {
    res.data = result;
  } else {
    res.err = new Error("Users don't exist.");
  }
  next();
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
  const result = UserService.search({id: req.params.id});
  if (result) {
    res.data = result;
  } else {
    res.err = new Error("Could't find a user.");
  }
  next();
}, responseMiddleware);

router.post('/', createUserValid, function(req, res, next) {
  const result = UserService.create(req.body);
  if (result) {
    res.data = result;
  } else {
    res.err = new Error("Couldn't create a user.");
  }
  next();
}, responseMiddleware);

router.put('/:id', updateUserValid, function(req, res, next) {
  const result = UserService.update(req.params.id, req.body);
  if (result) {
    res.data = result;
  } else {
    res.err = new Error("Couldn't update user's details.");
  }
  next();
}, responseMiddleware);

router.delete('/:id', function(req, res, next) {
  const result = UserService.delete(req.params.id);
  if (result) {
    res.data = result;
  } else {
    res.err = new Error("Error while deleting user.");
  }
  next();
}, responseMiddleware);

module.exports = router;