const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', function(req, res, next) {
  const result = FighterService.findAll();
  if (result) {
    res.data = result;
  } else {
    res.err = new Error("Fighters don't exist.");
  }
  next();
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
  const result = FighterService.search({id: req.params.id});
  if (result) {
    res.data = result;
  } else {
    res.err = new Error("Could't find a fighter.");
  }
  next();
}, responseMiddleware);

router.post('/', createFighterValid, function(req, res, next) {
  const result = FighterService.create(req.body);
  if (result) {
    res.data = result;
  } else {
    res.err = new Error("Couldn't create a fighter.");
  }
  next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, function(req, res, next) {
  const result = FighterService.update(req.params.id, req.body);
  if (result) {
    res.data = result;
  } else {
    res.err = new Error("Couldn't update fighter's details.");
  }
  next();
}, responseMiddleware);

router.delete('/:id', function(req, res, next) {
  const result = FighterService.delete(req.params.id);
  if (result) {
    res.data = result;
  } else {
    res.err = new Error("Error while deleting fighter.");
  }
  next();
}, responseMiddleware);

module.exports = router;